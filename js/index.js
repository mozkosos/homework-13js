let switchMode = document.getElementById('switchMode')
const header = document.querySelector('.header-dark')
let theme = document.getElementById('theme');

if (localStorage.getItem('style')=== null){
    localStorage.setItem('style','light')
}
if(localStorage.getItem('style')==='dark' && !header.classList.contains('header-dark') ){
    changeMode()
}
if (localStorage.getItem('style')=== 'light' && header.classList.contains('header-dark')){
    changeMode()
}

switchMode.onclick = function (){
    if (localStorage.getItem('style') === 'light'){
        localStorage.setItem('style', 'dark')
        changeMode()
    }else {
        localStorage.setItem('style', 'light')
        changeMode()
    }
}
function changeMode (){
    if (theme.getAttribute('href') === 'css/light-style.css'){
        theme.href = 'css/dark-style.css';
    }else{
        theme.href = 'css/light-style.css'
    }
}